(function () {
  'use strict';

  var Navigation = require('../navigation.page.js');
  var PageList = require('./page-list.page.js');
  var PageCreate = require('./page-create.page.js');
  var PageDetails = require('./page-details.page.js');
  var UserChooser = require('../../userchooser.page');
  var PageImprint = require('./page-imprint.page.js');
  var WidgetLayout = require('../widgets/widget-layout.page.js');
  var login = require('../../login.page.js');
  var testhelper = require('../../../testhelper.js');
  var components = require('../../components.page');

  describe('pages', function () {
    var pageList, pageCreate, pageDetails, navigation, pageImprint;

    beforeAll(function () {
      pageList = new PageList();
      pageCreate = new PageCreate();
      pageDetails = new PageDetails();
      navigation = new Navigation();
      pageImprint = new PageImprint();

      login.loginDefaultUser();
      testhelper.cancelTour();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('create a new page', function () {
      var suffix = Math.floor(Math.random() * 1000000);
      navigation.pages.click();
      testhelper.cancelTour();
      pageList.newButton.click();

      // invalid name
      pageCreate.page1.name.sendKeys('Company News');
      pageCreate.page1.name.sendKeys(protractor.Key.TAB);
      expect(pageCreate.page1.nameTaken.isPresent()).toBe(true);

      // valid name
      pageCreate.page1.name.clear();
      pageCreate.page1.name.sendKeys('Newpage' + suffix);
      pageCreate.page1.name.sendKeys(protractor.Key.TAB);
      expect(pageCreate.page1.nameTaken.isPresent()).toBe(false);

      pageCreate.page1.category.openDropdown();
      pageCreate.page1.category.selectOption('Company');

      pageCreate.page1.continueButton.click();

      // TODO user selection

      pageCreate.page2.continueButton.click();
      pageCreate.page3.createButton.click();
      expect(pageDetails.title.isPresent()).toBe(true);

      // delete created page
      pageDetails.options.settings.click();
      pageDetails.settings.deleteButton.click();
      components.modals.confirm.deleteButton.click();
    });

    it('add app file-library to page', function () {
      var pageName = 'TestPageToAddAppInDetailPage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);
      pageDetails.options.addApp.click();
      pageDetails.addApp.fileLibrary.click();
      pageDetails.addApp.save.click();
      pageDetails.fileLibraryTitle.getText().then(function (text) {
        expect(text).toBe('Documents');
      });
    });

    it('add groups and app to page', function () {
      var pageName = 'TestPageToAddGroupAndAppInDetailPage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);
      // add two new groups
      pageDetails.options.addGroup.click().then(function () {
        pageDetails.options.addGroup.click().then(function () {
          pageDetails.group.groupPanel.then(function () {
            expect(pageDetails.group.groupPanel.count()).toBe(2);
          });
        });
      });
    });

    it('should add current user as admin of page in superadminmode', function () {
      var pageName = 'TestAdminPageInSuperAdminmode' + Math.floor(Math.random() * 1000000);
      testhelper.createPage(pageName);

      login.logout();

      // login as robert lang
      login.login('robert.lang@coyo4.com', 'demo');
      testhelper.cancelTour();
      browser.get('/pages/' + pageName);
      testhelper.disableAnimations();

      // change to superadmin mode
      navigation.profileMenu.open();
      navigation.profileMenu.toggleModeratorMode();

      // edit admins of page
      pageDetails.options.settings.click();
      var userChooser = new UserChooser(element(by.model('$ctrl.pageOrigin')));
      userChooser.click();

      expect(userChooser.selectedTab.list.count()).toBe(1);

      userChooser.userTab.click();
      userChooser.userTab.search('Robert Lang');
      var rlUser = userChooser.userTab.list.get(0);
      rlUser.click();

      components.modals.confirm.confirmButton.click();

      var elem = pageDetails.settings.saveButton;
      browser.executeScript('arguments[0].scrollIntoView();', elem.getWebElement());
      elem.click();

      // leave superadmin mode
      navigation.profileMenu.open();
      navigation.profileMenu.toggleModeratorMode();

      // check if superadmin user is saved as admin of the page
      expect(pageDetails.options.settings.isPresent()).toBeTruthy();
      pageDetails.options.settings.click();
      userChooser.click();
      expect(userChooser.selectedTab.list.count()).toBe(2);

      userChooser.close();

      login.logout();
      login.loginDefaultAdminUser();
    });

    it('should open imprint', function () {
      var pageName = 'TestPageToAddAppInDetailPage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);

      // open page imprint
      pageDetails.options.imprint.click();

      expect(pageImprint.title.isPresent()).toBe(true);
      expect(pageImprint.admins.admins.count()).toBe(1);
    });

    it('should create widget in widget layout of imprint', function () {
      var pageName = 'TestPageToAddAppInDetailPage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);
      var widgetLayout = new WidgetLayout('plain');

      // open page imprint
      pageDetails.options.imprint.click();
      // activate edit mode
      pageImprint.options.edit.click();

      var widgetSlot = widgetLayout.getSlot();
      var widgetChooser = widgetSlot.widgetChooser;

      expect(widgetSlot.allWidgets.count()).toBe(0);

      // add widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Upcoming birthdays');
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // save widget
      pageImprint.options.save.click();
      expect(pageImprint.widgetSlot.hasOwnProperty('coyo-birthday-widget'));
    });

    it('should cancel creating widget in widget layout of imprint', function () {
      var pageName = 'TestPageToAddAppInDetailPage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);
      var widgetLayout = new WidgetLayout('plain');

      // open page imprint
      pageDetails.options.imprint.click();
      // activate edit mode
      pageImprint.options.edit.click();

      var widgetSlot = widgetLayout.getSlot();
      var widgetChooser = widgetSlot.widgetChooser;

      expect(widgetSlot.allWidgets.count()).toBe(0);

      // create widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Upcoming birthdays');
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // cancel edit mode
      pageImprint.options.cancel.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });

})();
