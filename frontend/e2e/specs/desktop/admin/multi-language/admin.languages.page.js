(function () {
  'use strict';

  var multiLanguage = require('./admin.multi-language.page');

  module.exports = {
    get: function () {
      multiLanguage.get();
      multiLanguage.navigationBar.languagesButton.click();
    },
    form: {
      table: $('.admin-languages table')
    }
  };

})();
