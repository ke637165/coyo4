(function () {
  'use strict';

  var login = require('../../login.page.js');
  var MobileNavigation = require('../mobile-navigation.page');

  describe('mobile navigation', function () {
    var mobileNavigation;

    beforeEach(function () {
      mobileNavigation = new MobileNavigation();
      login.loginDefaultUser();
    });

    it('open and close navigation', function () {
      // open navigation and test if navigation is open
      mobileNavigation.naviagtion.open();
      expect(mobileNavigation.naviagtion.navbar.isPresent()).toBeTruthy();

      mobileNavigation.naviagtion.close();
      expect(mobileNavigation.naviagtion.navbar.isPresent()).toBeFalsy();
    });
  });

})();
